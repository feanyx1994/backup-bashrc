# First Start script, run in terminal on fresh install
# sudo apt-get install screenfetch -y steam -y wine -y chromium-browser -y ; rm .bashrc ;  echo "screenfetch" >> .bashrc ; echo "alias Update='sudo apt-get update ; sudo apt-get upgrade -y ; sudo apt-get clean ; sudo apt-get autoclean ; sudo apt-get autoremove -y ; clear'" >> .bashrc ; nano .bashrc ; Update
screenfetch -n | lolcat
alias Update='sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get clean && sudo apt-get autoremove && clear && screenfetch | lolcat'
alias inst="sudo apt-get install -y"
alias remp="sudo aptitude purge"
alias search="apt-cache search"
alias ls="ls -al"
alias c="clear"
alias up="cd .."
alias disk="cd /"
alias trash="rm -fr ~/.Trash"
alias b="sh scripts/batterang.sh"
alias wifi="nmtui"
